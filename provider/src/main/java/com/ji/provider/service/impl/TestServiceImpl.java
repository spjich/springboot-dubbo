package com.ji.provider.service.impl;

import com.ji.provider.dao.TestDao;
import com.ji.provider.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2016/12/27
 * Version:1.1.0
 */
@Service
@Transactional
public class TestServiceImpl implements TestService {
    @Autowired
    private TestDao dao;

    @Override
    public String getId() {
        return dao.getId();
    }
}
