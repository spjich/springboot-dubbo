package com.ji.provider.controller;

import com.ji.provider.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Title:
 * Description:
 * Author:吉
 * Since:2016/12/27
 * Version:1.1.0
 */
@RestController
public class TestController {
    @Autowired
    private TestService service;
    @Autowired
    private StringRedisTemplate template;

    @RequestMapping("/db")
    public String db() {
        return service.getId();
    }

    @RequestMapping("redis")
    public String redis() {
        String key = "test";
        template.opsForValue().set(key, "testRedis");
        String value = template.opsForValue().get(key);
        template.delete(key);
        return value;
    }
}
