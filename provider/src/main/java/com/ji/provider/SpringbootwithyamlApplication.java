package com.ji.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@ImportResource("classpath:privoder.xml")
@SpringBootApplication
public class SpringbootwithyamlApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootwithyamlApplication.class);
    }
}
